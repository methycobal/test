import React from "react"
import PropTypes from "prop-types"

export default function HTML(props) {
  return (
    <html {...props.htmlAttributes}>
      <head>
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        {props.headComponents}
      </head>
      <body {...props.bodyAttributes}>
        {props.preBodyComponents}
        <noscript key="noscript" id="gatsby-noscript">
          This app works best with JavaScript enabled.
        </noscript>
        <div
          key={`body`}
          id="___gatsby"
          dangerouslySetInnerHTML={{ __html: props.body }}
        />
        {props.postBodyComponents}
        <script
          dangerouslySetInnerHTML={{
            __html: [
              // 'var srcs = ["//cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js", "//vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js", "//code.jquery.com/jquery-3.3.1.slim.min.js", "//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"]',
              'var srcs = ["//code.jquery.com/jquery-3.3.1.slim.min.js", "//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"];',
              "srcs.forEach(function (src) {",
              'var head = document.head || document.getElementsByTagName("head")[0];',
              'var script = document.createElement("script");',
              "script.src = src;",
              "script.defer = true;",
              "head.appendChild(script);",
              "});",
            ].join(process.env.NODE_ENV === "production" ? "" : "\n"),
          }}
        />
      </body>
    </html>
  )
}

HTML.propTypes = {
  htmlAttributes: PropTypes.object,
  headComponents: PropTypes.array,
  bodyAttributes: PropTypes.object,
  preBodyComponents: PropTypes.array,
  body: PropTypes.string,
  postBodyComponents: PropTypes.array,
}
