module.exports = {
  siteMetadata: {
    title: `Methycobal Thailand`,
    description: `เมทิโคบอล เป็นผลิตภัณฑ์ Mecobalamin ซึ่งบริษัท เอไซ จำกัด ได้พัฒนาขึ้นมา โดยอาศัย เทคโนโลยีต้นแบบของบริษัท เพื่อใช้เป็นยาสำหรับรักษาโรคของระบบประสาทส่วนปลาย ในแง่ของชีวเคมี Mecobalamin เกี่ยวข้องกับปฏิกิริยา Transmethylation ในฐานะที่เป็นวิตามิน บี12`,
    // author: `@ilumin`,
  },
  plugins: [
    {
      resolve: "gatsby-plugin-netlify",
    },
    // {
    //   resolve: "gatsby-plugin-prefetch-google-fonts",
    //   options: {
    //     fonts: [
    //       {
    //         family: `Sarabun`,
    //       },
    //       // {
    //       //   family: `Pridi`,
    //       // },
    //       // {
    //       //   family: `Kanit`,
    //       //   variants: [`500`],
    //       // },
    //     ],
    //   },
    // },
    // {
    //   resolve: "gatsby-plugin-web-font-loader",
    //   options: {
    //     google: {
    //       families: ["Sarabun"],
    //     },
    //   },
    // },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-nprogress`,
      options: {
        color: `tomato`,
        showSpinner: true,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-plugin-sass`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/logo.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    `gatsby-plugin-offline`,
    `gatsby-plugin-zopfli`,
    {
      resolve: `gatsby-plugin-recaptcha`,
      options: {
        async: true,
        defer: true,
        args: `?onload=onloadCallback&render=explicit`,
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-150373456-1",
      },
    },
  ],
}
